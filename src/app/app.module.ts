import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { CadastroComponent } from './component/cadastro/cadastro.component';
import { ImportacaoComponent } from './component/importacao/importacao.component';
import { MensagemComponent } from './component/mensagem/mensagem.component';

import { NgxMaskModule, IConfig } from 'ngx-mask';
import { EdicaoComponent } from './component/edicao/edicao.component';
import { CSV2JSONModule } from 'angular2-csv2json';



export var options: Partial<IConfig> | (() => Partial<IConfig>);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CadastroComponent,
    ImportacaoComponent,
    MensagemComponent,
    EdicaoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CSV2JSONModule,    
    NgxMaskModule.forRoot(options)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
