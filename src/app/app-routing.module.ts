import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { CadastroComponent } from './component/cadastro/cadastro.component';
import { ImportacaoComponent } from './component/importacao/importacao.component';
import { MensagemComponent } from './component/mensagem/mensagem.component';
import { EdicaoComponent } from './component/edicao/edicao.component';

const routes: Routes = [
  { path: '', component:HomeComponent},
  { path: 'cadastro', component: CadastroComponent},
  { path: 'edicao/:id', component: EdicaoComponent},
  { path: 'importacao', component: ImportacaoComponent},
  { path: 'mensagem', component: MensagemComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
