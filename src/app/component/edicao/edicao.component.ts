import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RemetenteService } from 'src/app/service/remetente.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edicao',
  templateUrl: './edicao.component.html',
  styleUrls: ['./edicao.component.css']
})
export class EdicaoComponent implements OnInit {

  id: number;
  remetente: any = {};

  constructor(private router: Router, private route: ActivatedRoute, private remetenteService: RemetenteService) { }

  private headers = new Headers({
    'Content-Type': 'application/json'
  });

  ngOnInit() {
    this.route.params.subscribe(params => this.id = params['id']);
    this.getRemetenteById();
  }

  getRemetenteById(){
    this.remetenteService.getRemetenteById(this.id).subscribe((remetenteAux: any) => {
      this.remetente = remetenteAux;      
    });
  }

  updateRemetente(form: NgForm){
    this.remetenteService.updateRemetente(this.remetente).subscribe(()=>{
      this.router.navigate([''])
    })
  }

}
