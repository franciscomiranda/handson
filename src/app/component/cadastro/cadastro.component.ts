import { Component, OnInit } from '@angular/core';
import { Remetente } from 'src/app/class/remetente';
import { RemetenteService } from 'src/app/service/remetente.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {
  
  id: number;  
  remetente = {} as Remetente;
  arrayRemetente: Remetente[]; 

  constructor(private remetenteService: RemetenteService) { }

  ngOnInit() {
    this.getRemetentes();
  }

  private headers = new Headers({
    'Content-Type': 'application/json'
  });

  getRemetentes(){
    this.remetenteService.getRemetentes().subscribe((arrayRemetente: Remetente[]) => {
      this.arrayRemetente = arrayRemetente;
    });
  }

  getRemetenteById(){
    this.remetenteService.getRemetenteById(this.id).subscribe((remetenteAux: any) => {
      this.remetente = remetenteAux;      
    });
  }

  saveRemetente(form: NgForm){
    if(this.remetente.id !== undefined){
      this.remetenteService.updateRemetente(this.remetente).subscribe(()=>{
        this.limparForm(form);
      });
    }else{
      this.remetenteService.createRemetente(this.remetente).subscribe(()=>{
        this.limparForm(form);
      })
    }
  }


  deletarRemetente(remetente: Remetente){
    this.remetenteService.deleteRemetente(remetente).subscribe(()=>{
      this.getRemetentes();
    })
  }

  isEmpty(){
    let isEmpty = false;
    if(this.arrayRemetente.length==0){
      isEmpty=true;
    }
    return isEmpty;
  }

  limparForm(form: NgForm){
    this.getRemetentes();
    form.resetForm;
    this.remetente = {} as Remetente;
  }

}
