
export class Remetente {
    id: number;
    nome: string;
    cpf: string;
    email: string;
    telefone: string;
}
