

export class Destinatario {
    id: number;
    nome: string;
    cpf: string;
    email: string;
    telefone: string;
}
