import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Remetente } from '../class/remetente';
import { convertMetaToOutput } from '@angular/compiler/src/render3/util';

@Injectable({
  providedIn: 'root'
})
export class RemetenteService {

  constructor(private httpClient: HttpClient) { }

  url='http://localhost:8080/remetente/'

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  getRemetenteById(id: number): Observable<Remetente>{
    return this.httpClient.get<Remetente>(this.url + 'listaid/'+id).pipe(retry(2), catchError(this.handleError));
  }

  getRemetentes(): Observable<Remetente[]> {
    return this.httpClient.get<Remetente[]>(this.url + 'lista').pipe(retry(2), catchError(this.handleError));
  }

  createRemetente(remetente: Remetente): Observable<Remetente> {
    return this.httpClient.post<Remetente>(this.url+'create', JSON.stringify(remetente), this.httpOptions).pipe(retry(2), catchError(this.handleError));
  }

  updateRemetente(remetente: Remetente): Observable<Remetente> {
    return this.httpClient.put<Remetente>(this.url+'update/'+remetente.id, JSON.stringify(remetente), this.httpOptions).pipe(retry(2), catchError(this.handleError));
  }

  deleteRemetente(remetente: Remetente) {
    return this.httpClient.delete<Remetente>(this.url+'delete/'+ remetente.id, this.httpOptions).pipe(retry(2), catchError(this.handleError))
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
