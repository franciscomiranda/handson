import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Destinatario } from '../class/destinatario';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DestinatarioService {

  constructor(private httpClient: HttpClient) { }

  url='http://localhost:8080/destinatario/'

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })    
  }

  insertListaCsv(lista): Observable<Destinatario>{
    return this.httpClient
      .post<Destinatario>('create', JSON.stringify(lista), this.httpOptions).pipe(retry(2), catchError(this.handleError));
    //return this.httpClient.post<Destinatario[]>(this.url+'create', JSON.stringify(lista), this.httpOptions).pipe(retry(2), catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
